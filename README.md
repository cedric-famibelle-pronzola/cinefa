# Projet PHP "Cinefa"

## Pour l'utiliser en local ou le modifier, vous devrez cloner le dépôt (repository) ou le télécharger, mais vous devrez également :

* *Vous munir de "wamp", "xamp" ou "mamp".*
* *Créer une base de données nommée 'cinefa' dans phpmyadmin*.
* *Y importer le fichier 'cinefa.sql' qui se trouve dans le dossier 'bdd'.*
 

## Accès à la base de données 

```
adresse : localhost
login : root
mot de passe : password
database : cinefa
```

## Vous pouvez modifier les informations de connexions :

* /connect/config.php *`(ligne 3 à 5)`*
```php
 3   define('DB_SERVER', 'localhost'); // adresse
 4   define('DB_USER', 'root'); // login
 5   define('DB_PASS', 'password'); // mot de passe
```


## Pour modifier le nom de la base de données :

* /connect/connexion.php *`(ligne 4)`*
```php
4  $db_name = 'cinefa'; // nom de la base de données
```
***

# Consignes liées au projet 'Cinefa'

## Partie 1

#### Faire une page 'réalisateurs' :
- Je peux voir tous les réalisateurs
- Je peux cliquer sur un réalisateur et accéder à sa fiche

#### Faire une page 'fiche réalisateur' :
- Je peux voir les informations générales sur le réalisateur
- Je peux voir les 3 derniers films qu’il a réalisés

#### Faire une page 'acteurs' :
- Je peux voir tous les acteurs
- Je peux cliquer sur un acteur et accéder à sa fiche
#### Faire une page 'fiche acteur' :
- Je peux voir les informations générales sur l’acteur
- Je peux voir les 3 derniers films dans lequel il a joué
#### Faire une page 'films' :
- Je peux voir tous les films
- Je peux cliquer sur un film et accéder à sa fiche
#### Faire une page 'fiche film' :
- Je peux voir les infos générales sur le film
- Je peux voir le réalisateur
- Je peux voir tous les acteurs de ce film

## Partie 2

#### Rajouter ces fonctionnalités :
- Sur la page qui présente les acteurs, je dois pouvoir faire une recherche (texte) sur un acteur.
- Sur la page qui présente les réalisateurs, je dois pouvoir faire une recherche (texte) sur un réalisateur.
- Sur la page qui présente les films, je dois pouvoir faire une recherche (texte) sur un film.

## Partie 3

#### Rajouter ces fonctionnalités :
- Je peux ajouter une note sur un film (ou plusieurs).
- Sur la page des films, je peux afficher tous les films ou uniquement les films notés.
- On peut voir la note moyenne du film pour tous les utilisateurs
- Je peux créer une catégorie de film. Par exemple : a voir / déjà vu / horreur / action etc…. . Une catégorie contient :
    * un titre
    * une date de création
    * la liste des films de cette catégorie
- Je peux supprimer un film de ma catégorie
- Je peux modifier le titre de ma catégorie
- Je peux supprimer ma catégorie

## Partie 4

#### Rajouter ces fonctionnalités :
- Je dois pouvoir gérer la connexion à un compte utilisateur.
- Même si je ferme mon navigateur, je dois pouvoir conserver mes données si je rouvre celui-ci.